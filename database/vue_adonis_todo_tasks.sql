-- MySQL dump 10.13  Distrib 8.0.17, for macos10.14 (x86_64)
--
-- Host: localhost    Database: vue_adonis_todo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_project_id_foreign` (`project_id`),
  CONSTRAINT `tasks_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,1,'Initialise Vue client project ',NULL,'2019-08-27 12:34:09','2019-08-27 12:34:47'),(2,1,'Add Vue packages dependency',NULL,'2019-08-27 12:34:40','2019-08-27 12:34:40'),(3,1,'Add mandatory features',NULL,'2019-08-27 12:35:09','2019-08-27 12:35:09'),(4,1,'Add unit test cases',NULL,'2019-08-27 12:35:18','2019-08-27 12:35:55'),(6,1,'Add unit test functionalities',NULL,'2019-08-27 12:36:14','2019-08-27 12:36:14'),(7,1,'Add integration test cases',NULL,'2019-08-27 12:36:30','2019-08-27 12:36:30'),(8,1,'Add integration functionalities',NULL,'2019-08-27 12:36:39','2019-08-27 12:36:39'),(9,1,'Build, test and deploy',NULL,'2019-08-27 12:37:09','2019-08-27 12:37:09'),(10,2,'Initialise Adonis server project',NULL,'2019-08-27 12:37:47','2019-08-27 12:37:47'),(11,2,'Add Adonis packages dependency',NULL,'2019-08-27 12:37:58','2019-08-27 12:37:58'),(12,2,'Add necessary features',NULL,'2019-08-27 12:38:13','2019-08-27 12:38:13'),(13,2,'Add unit test cases',NULL,'2019-08-27 12:38:21','2019-08-27 12:38:21'),(14,2,'Add unit test functionalities',NULL,'2019-08-27 12:38:28','2019-08-27 12:38:28'),(15,2,'Add integration test cases',NULL,'2019-08-27 12:38:35','2019-08-27 12:38:35'),(16,2,'Add integration functionalities',NULL,'2019-08-27 12:38:42','2019-08-27 12:38:42'),(17,2,'Build, test and deploy',NULL,'2019-08-27 12:38:50','2019-08-27 12:38:50'),(18,3,'Step 1: Launch the RDS Instances in a VPC by Using the CloudFormation Template',NULL,'2019-08-27 12:40:01','2019-08-27 12:40:01'),(19,3,'Step 2: Install the SQL Tools and AWS Schema Conversion Tool on Your Local Computer',NULL,'2019-08-27 12:40:09','2019-08-27 12:40:09'),(20,3,'Step 3: Test Connectivity to the Oracle DB Instance and Create the Sample Schema',NULL,'2019-08-27 12:40:17','2019-08-27 12:40:17'),(21,3,'Step 4: Test the Connectivity to the Aurora MySQL DB Instance',NULL,'2019-08-27 12:40:30','2019-08-27 12:40:30'),(22,3,'Step 5: Use the AWS Schema Conversion Tool (AWS SCT) to Convert the Oracle Schema to Aurora MySQL',NULL,'2019-08-27 12:40:38','2019-08-27 12:40:38'),(23,3,'Step 6: Validate the Schema Conversion',NULL,'2019-08-27 12:40:46','2019-08-27 12:40:46'),(24,3,'Step 7: Create a AWS DMS Replication Instance',NULL,'2019-08-27 12:41:01','2019-08-27 12:41:01'),(25,3,'Step 8: Create AWS DMS Source and Target Endpoints',NULL,'2019-08-27 12:41:09','2019-08-27 12:41:09'),(26,3,'Step 9: Create and Run Your AWS DMS Migration Task',NULL,'2019-08-27 12:41:16','2019-08-27 12:41:16'),(27,3,'Step 10: Verify That Your Data Migration Completed Successfully',NULL,'2019-08-27 12:41:23','2019-08-27 12:41:23'),(28,3,'Step 11: Delete Walkthrough Resources',NULL,'2019-08-27 12:41:30','2019-08-27 12:41:30');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-27 12:48:09
