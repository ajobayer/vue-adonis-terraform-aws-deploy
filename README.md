# System Requirements on both client and server side

## Installation Node.js >= 8.0.0 && npm >= 3.0.0
```bash
sudo apt-get install nodejs
```

# Vue Client Side Configuration

## install all the dependencies
```bash
npm install
```
## Run server
```bash
npm run build
npm run serve
```



# Adonis Server side API Configuration
This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Adonis CLI installation
```bash
npm i -g @adonisjs/cli
```

## Setup
Use the adonis command to install the blueprint
```bash
adonis new server --api-only
```
or manually clone the repo and then run `npm install`.
```bash
npm install
cp .env.example .env
```

### Migrations
Run the following command to run startup migrations.
```bash
adonis migration:run --force
```

## Generate API Key
```
adonis key:generate
```

## Run Adonis API
```bash
node server.js
```


# Database Migration (Run follwoing commands from adonis server)

## Import 
```bash
mysql -h  ${PMDataBaseEndpoint} -P 3306 -u ${PMDataBaseUserName} -p${PMDataBasePassword} < vue_adonis_todo_projects.sql
```

## Import
```bash
mysql -h  ${PMDataBaseEndpoint} -P 3306 -u ${PMDataBaseUserName} -p${PMDataBasePassword} < vue_adonis_todo_tasks.sql
```
